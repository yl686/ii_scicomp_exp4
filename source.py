import numpy as np
"""
Source codes for studying potential energy surfaces of many-atom systems
modelled by pairwise potentials.

Classes:
    System              provides interface for performing calculation
    Vector(np.ndarray)  3d vector objects with usual vector operations
    Potential           system potential with capacity for different
                        forms of pair potentials

"""

"""preset potential functions"""

class System:
    """
    A many-body system with the internal energy modelled by pair potentials.
    Povides interface for the optimisation of the system of Vector objects with
    Potential describing the interactions.

    Variables:
      space            list  array of particles as position Vectors
      elements         list  the identity of the ith particle as strings.
      potential  Potential.  system potential - //comes with two presets, 'LJ'
                             and 'Morse', but can manually provide a Potential
                             object//

    Methods:
      from_xyz(cls,filename,potential) instantiation from .xyz file
      minimum(self)       all-containing method returning optimum geometry
      get_grad(self,vectors,du)  obtains the list of gradient vectors
      descent(self,vectors,grad,ds)  takes a small step downhill for all n particles
      out_xyz(self)       creates .xyz file to record the optimised geometry
    """


    def __init__(self,particles,elements,potential):
        """
        System of len(particles) bodies.
        particles: list of Cartesian coordinates indicating the positions of particles
        elements: identity of the atoms in the same order
        potential: Potential instance object for the pair potential to be used
        n: number of atoms
        """
        self.space = [Vector(item) for item in particles]
        self.elements = elements
        self.potential = potential
        self.n = len(elements)

    @classmethod
    def from_xyz(cls,filename,potential):
        """
        Instantiation from a .xyz file specifying the particle positions.
        Potential is a Potential instance object required for the instantiation.
        Note that this is not the only way intended for the instantiation.
        """
        elements = []
        positions = []

        with open(filename,"r") as f:
            f.readline() # number of atoms
            f.readline() # comment line
            lines = f.readlines()

            for line in lines:
                l = line.split() # [element x y z]
                elements.append(l[0])
                coord = [float(n) for n in l[1:]]
                positions.append(coord)

        return cls(positions,elements,potential)

    def __str__(self):
        ls = [str(v) for v in self.space]
        str_to_print = "\n".join(ls)
        return str_to_print

    def __getitem__(self,index):
        return self.space[index]

    @property
    def r(self):
        """calculates the difference vectors ri-rj"""

        n = self.n
        r_diff = []
        # n*n possible combinations of r_diff[i][j] = ri - rj
        for i in range(n):
            r_diff.append([])
            for j in range(n):
                r_diff[i].append(self[i] - self[j])
        return r_diff

    @property
    def energy(self):
        return self.energy()

    def energy(self,r=None):
        """Calculates the internal energy"""
        n = self.n
        p = self.potential
        if r is None: # when r has already been calculated,
                      # it can be explicitly parsed to avoid
                      # unnecessary looping.
            r = self.r
        r_len = []
        for i in range(n):
            for j in range(i+1,n,1):
                r_len.append(r[i][j].length)
        energy = p.total_energy(r_len)
        return energy

    def get_grad(self,r=None,du=None):
        """
        calculates the gradient vector for each particle.
        This is done by 1) creating n * n array of difference vectors
        ri-rj, 2) calculating the derivatives in  direction of the vectors,
        and 3) adding up all the contributions from all the pair potentials.
        It seems like a double counting but that doesn't matter.
        """
        if r is None:
            r = self.r # separation vectors
        if du is None:
            du = self.potential.du
        n = self.n
        for i in range(n):
            for j in range(n):
                if i==j:
                    r[i][j] = 0
                else:
                    ri = r[i][j] # particular separation vector
                    r[i][j] = (du(ri.length)/ri.length)*ri
        # now r has been updated to the array of gradient vector components
        # there are total (n-1)*(n-1) of nonzero items and n of self-to-self
        lst_grad = []
        for row in r:
            dr = Vector([0,0,0])
            for rj in row:  # gradient vector of particle ri = sum of grad rij
                dr = dr + rj
            lst_grad.append(dr)

        return lst_grad

    def descent(self,grad,ds=1e-4):
        """Takes a small step downhill for each particle.
        There is still room for better optimisation, since this would
        only allow the local minimum to be found.
        """
        for i in range(self.n):
            self.space[i] = self.space[i] - grad[i]*ds

    def minimum(self,ds=1e-3,tol=1e-6):
        """Finds the energy minimum."""
        n = self.n
        r = self.r
        energy = self.energy(r)
        cycle = 0
        for i in range(100000): # a breakproof
            grad_r = self.get_grad(r)
            self.descent(grad_r,ds)
            r = self.r
            new_energy = self.energy(r)
            for j in range(n):
                if grad_r[j].length > tol:
                    break
                cycle += i
                return cycle,energy
            energy = new_energy

        # If the loop does not return over 1e05
        print("The system does not converge in 1e05 cycles.\n")

    def out_xyz(self,filename="out.xyz"):
        """Records the current geometry as .xyz file.
        Ideally, this is to be called after minimum().
        """
        n = self.n
        with open(filename,"w") as f:
            f.write("{}\n".format(n))
            f.write("Optimum geometry with {} potential\n".format(self.potential))
            for i in range(n):
                pos = self.space[i]
                element = self.elements[i]
                f.write(element+"  {:.4f}  {:.4f}  {:.4f}\n".format(pos[0],
                                                                pos[1],pos[2]))



class Potential:
    """
    Implements calculation of the system energy by iterating through pairs,
    and the calculation of the grad for each atom.

    Variables:
      name         str the name of the potential
      u            fn  takes in separation r and returns the pair interaction E
      du           fn  takes in separatoin r and returns the derivative

    Methods:
    total_energy(self,list)  returns the total energy of the pair potential system
                             containing the vectors given by the list
    """
    def __init__(self,name,u,du,simplify=False,*args,**kwargs):
        """simplify is set True if u,du requires more than one argument"""
        self.name = name
        if simplify:
            self.u = _simplify(u,args,kwargs)
            self.du = _simplify(du,args,kwargs)
        else:
            self.u = u
            self.du = du

    def __str__(self): # when print potential, print the name of the potential
        return self.name

    def total_energy(self,list):
        """returns total energy of the system calculated from the list of r"""
        energy = 0
        for r in list:
            energy += self.u(r)
        return energy


class Vector(np.ndarray):
    """A 3D Cartesian vector."""
    def __new__(cls, coord=[0,0,0]):
        ar = np.array(coord,dtype=float)
        # instantiate the base class as a Vector instance
        return np.ndarray.__new__(cls,shape=(3,),dtype=float,buffer=ar)

    @property
    def length(self):
        return np.sqrt(np.dot(self,self))


"""==========================================="""
"""internal functions"""
def _simplify(fn,args,kwargs):
    """wrapper to simplify Morse fns"""
    def simple_fn(r):
        return fn(r,*args,**kwargs)
    return simple_fn
