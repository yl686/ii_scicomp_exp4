import numpy as np
import os
from source import System, Potential, Vector
"""
Programme to find a (local) optimum geometry of a system of pair potential
by taking small steps down along the steepest direction.
The user can choose either LJ or Morse potential,
or to provide a new potential and its derivative as Python fns.
"""

# potential function library
# def area
def u_lj(r):
    energy = 4 * (r**(-12) - r**(-6))
    return energy

def du_lj(r):
    de = 4 * (-12*r**(-13) + 6*r**(-7))
    return de

def u_morse(r,re):
    energy = (1-np.exp(-r+re))**2
    return energy

def du_morse(r,re):
    de = 2*(1-np.exp(-r+re))*(np.exp(-r+re))
    return de

# catalog
library = {"LJ":(u_lj,du_lj),"MORSE":(u_morse,du_morse)}

# Some strings
info_message = "====================================================================\n\n\
    Exercise 4: Optimisation \n\n    written by Yeha Lee \n \
    yl686@cam.ac.uk \n    free to use, modify and redistribute \n    (UNLESS \
you are re-handing in as your Scientific Computing submission)\n\
====================================================================\n"

welcome_message = "\nPlease choose your potential from:\n\
    1) LJ\n    2) Morse\n    3) Custom\nType in a number:"

system_message = "\nSetting up the many-body system.\nThis can be done from\n\
    1) an existing xyz file\n    2) an xyz file of your choice."
# Some functions
def setup_potential(potential_name,*args,**kwargs):
    # Look up the pair potential fns
    name = potential_name.upper()
    simplify = False
    try:
        u,du = library[name]
    except KeyError:
        print("\nYour potential doesn't seem to be in the library.\n\Are you sure you have added the functions?\n")
        return "missing_key"

    # set up the potential
    if name == "MORSE":
        simplify = True
    potential = Potential(potential_name,u,du,simplify=simplify,*args,**kwargs)
    return potential

def setup_system(potential,f=True,filename=None,preset=None):
    pre_files = ["test1.xyz","test2.xyz","test3.xyz","test4.xyz"]
    if not isinstance(potential,Potential):
        print("\nOops! Something went wrong with your potential setup.\nPlease try again.\n")
        return
    try:
        if f:
            f_xyz = filename
        else:
            f_xyz = pre_files[int(preset)-1]
    except:
        print("\nPlease provide the right filename.\n")
        return

    s = System.from_xyz(f_xyz,potential)
    return s

# find the minimum
def optimise(system):
    energy = system.energy()
    print("\nInitial energy of the system: {:.4f}".format(energy))
    print("\nOptimising the geometry by taking small steps downhill.")
    print("Set the step size and relative tolerance. \nWarning: the system may not converge.\nIf you wish to use the default value\
(0.005 1e-5), just type Enter.")
    ds_tol = input("(step_size tolerance): ")
    if ds_tol == "":
        step_size, tol = 0.005, 1e-5
    else:
        ds, tol = ds_tol.split()
        step_size = float(ds)
        tol = float(tol)
    print("\nFinding the minimum...\n")
    try:
        n, energy = system.minimum(ds=step_size,tol=tol)
    except:
        print("Your system did not reach the optimum.")
    print("The system converged after {} steps\n".format(n))
    print("    ***  The minimum energy found: {:.4f}  ***\n".format(energy))

def outfile(system):
    filename = input("Please provide the name of the output file to be created:")
    system.out_xyz(filename)
    print("The current system has been saved as {}.".format(filename))
    return filename


"================================================="
# execution part
print(info_message)
potential_n = input(welcome_message)
for i in range(5):
    if potential_n in ["1","2","3"]:
        break
    if i >= 5:
        ("Go grab a coffee and come back try again.")
        exit()
    else:
        print("Invalid option. Try again!")

#potential setup
info = []
kwargs = {}
if potential_n == "1":
    name = "LJ"
elif potential_n == "2":
    name = "Morse"
    re = input("Indicate r_e with a number:")
    kwargs["re"] = float(re)
else:
    print("Make sure you have added the fns to the library.")
    name = input("Type in the name:")
    additional_info = input("Provide the required arguments:").split()
    for n in additional_info:
        info.append(float(n))
potential = setup_potential(name,*info,**kwargs)

#system setup
kwargs = {}
print(system_message)
if_preset = input("Choose with a number:")
if if_preset == "1":
    kwargs["f"] = False
    try:
        kwargs["preset"] = int(input("Choose the preset file from 1, 2, 3, 4:"))
    except:
        print("Please try with a valid number.")
        exit()
elif if_preset == "2":
    kwargs["filename"] = input("Please give the filename:")
print("Initialising the system...")
try:
    system = setup_system(potential,**kwargs)
except:
    print("Something went wrong with your system configuration. Try again.")
    exit()

# running the simulation
print("Optimising the geometry of a {name} system of {n} bodies.".format(name=name,n=system.n))
optimise(system)

# accessing the results
save = input("Do you wish to export the optimised geometry as .xyz file? (y/n):").lower()
if save == "y":
    filename = outfile(system)

display = input("Do you wish to display the optimised geometry? (for Linux system) (y/n):").lower()
if display == "y":
    try:
        os.system("vmd {}".format(filename))
    except:
        print("Creating an .xyz file first.")
        filename = outfile(system)
        os.system("vmd {}".format(filename))
